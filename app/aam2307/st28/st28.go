package aam230728

import (
	"Lab5/aam2307/st28/api"
	"Lab5/aam2307/st28/media"
	"Lab5/aam2307/st28/menu"
	"Lab5/rest"
	"fmt"
	"strconv"
	"strings"
)

func Main() {
	stIndex, status := getMinePrefixNumber("[2307-28]")

	if status {
		mediaApiClient := api.MediaApiClient{StIndex: strconv.Itoa(stIndex)}
		mediaResource := media.MediaResource{MediaApiClient: &mediaApiClient}
		menu := menu.Menu{Media: mediaResource}
		menu.ShowMenu()
	} else {
		fmt.Println("Не удалось найти клиента")
	}
}

func getMinePrefixNumber(moduleIdent string) (int, bool) {
	sts := rest.GetAny(rest.DoRequest("GET", "", "", nil)).(map[string]any)["sts"]

	for _, st := range sts.([]any) {
		if strings.Contains(st.([]any)[1].(string), moduleIdent) {
			return int(st.([]any)[0].(float64)), true
		}
	}
	return 0, false
}
