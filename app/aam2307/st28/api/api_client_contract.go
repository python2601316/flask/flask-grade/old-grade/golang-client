package api

type MediaApiClientContract interface {
	GetFeed() []*Publication
	GetPublicationById(id int) *Publication
	CreatePublication(publication Publication) bool
	UpdatePublicationById(id int, publication Publication) bool
	DeletePublicationById(id int) bool
	ClearFeed() bool
}
