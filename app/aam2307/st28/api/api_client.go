package api

import (
	"Lab5/rest"
	"encoding/json"
	"fmt"
	"strconv"
)

type MediaApiClient struct {
	StIndex string
}

type GetFeedResponse struct {
	Data []*Publication `json:"list"`
}

type GetPublicationResponse struct {
	Data *Publication `json:"item"`
}

type GetActionStatusResponse struct {
	Status bool `json:"success"`
}

func (c *MediaApiClient) GetFeed() []*Publication {
	var res GetFeedResponse
	c.SendRequest("GET", "feed", nil, &res)
	return res.Data
}

func (c *MediaApiClient) GetPublicationById(id int) *Publication {
	var res GetPublicationResponse
	c.SendRequest("GET", "feed/"+strconv.Itoa(id), nil, &res)
	return res.Data
}

func (c *MediaApiClient) CreatePublication(publication Publication) bool {
	var res GetActionStatusResponse
	c.SendRequest("POST", fmt.Sprintf("feed/"), publication, &res)
	return res.Status
}

func (c *MediaApiClient) UpdatePublicationById(id int, publication Publication) bool {
	var res GetActionStatusResponse
	c.SendRequest("POST", "feed/"+strconv.Itoa(id), publication, &res)
	return res.Status
}

func (c *MediaApiClient) DeletePublicationById(id int) bool {
	var res GetActionStatusResponse
	c.SendRequest("DELETE", "feed/"+strconv.Itoa(id), nil, &res)
	return res.Status
}

func (c *MediaApiClient) ClearFeed() bool {
	var res GetActionStatusResponse
	c.SendRequest("DELETE", "clear-feed", nil, &res)
	return res.Status
}

func (c *MediaApiClient) SendRequest(method string, endpoint string, data any, responseTo any) any {
	return json.Unmarshal(
		rest.GetBytes(rest.DoRequest(method, "st"+c.StIndex+"/", endpoint, data)),
		&responseTo,
	)
}
