package api

import (
	"fmt"
	"os"
	"strconv"
)

type Publication struct {
	Id          int    `json:"id" validate:"required"`
	Type        string `json:"type" validate:"required"`
	Title       string `json:"title" validate:"required"`
	Text        string `json:"text" validate:"required"`
	PublishedAt string `json:"published_at"`
	Source      string `json:"source"`
	Author      string `json:"author"`
	OutDate     string `json:"out_date"`
}

func (p *Publication) Show() {
	fmt.Println("------------------------")
	fmt.Println("Id: " + strconv.Itoa(p.Id))
	fmt.Println("Тип публикации: " + p.Type)
	fmt.Println("Заголовок: " + p.Title)
	fmt.Println("Содержание: " + p.Text)
	fmt.Println("Дата публикации: " + p.PublishedAt)
	fmt.Println("Источник: " + p.Source)
	fmt.Println("Автор: " + p.Author)
	fmt.Println("Дата выхода: " + p.OutDate)
}

func (p *Publication) Read() (Publication, bool) {
	var chosenType int
	fmt.Print("Выберите тип публикации: 1 - Статья, 2 - Новость, 3 - Анонс: ")
	fmt.Fscan(os.Stdin, &chosenType)

	if chosenType < 1 || chosenType > 3 {
		fmt.Println("Некорректный тип")
		return Publication{}, false
	}

	var title, text, publishedAt, source, author, outDate, pType string
	fmt.Print("Заголовок: ")
	fmt.Fscan(os.Stdin, &title)

	fmt.Print("Содержание: ")
	fmt.Fscan(os.Stdin, &text)

	switch chosenType {
	case 1:
		fmt.Print("Автор: ")
		fmt.Fscan(os.Stdin, &author)
		pType = "article"
	case 2:
		fmt.Print("Дата публикации: ")
		fmt.Fscan(os.Stdin, &publishedAt)

		fmt.Print("Источник: ")
		fmt.Fscan(os.Stdin, &source)

		pType = "news"
	case 3:
		fmt.Print("Дата выхода: ")
		fmt.Fscan(os.Stdin, &outDate)
		pType = "announcement"
	}

	return Publication{
		Id:          0,
		Type:        pType,
		Title:       title,
		Text:        text,
		PublishedAt: publishedAt,
		Source:      source,
		Author:      author,
		OutDate:     outDate,
	}, true
}
