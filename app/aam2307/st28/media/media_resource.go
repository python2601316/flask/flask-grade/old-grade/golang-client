package media

import (
	"Lab5/aam2307/st28/api"
	"fmt"
	"os"
)

type MediaResource struct {
	MediaApiClient api.MediaApiClientContract
}

func (media *MediaResource) AddElement() {
	newPub := api.Publication{}
	publication, status := newPub.Read()

	if status == false {
		return
	}

	if media.MediaApiClient.CreatePublication(publication) {
		fmt.Println("Публикация успешно создана")
	} else {
		fmt.Println("Не удалось создать публикацию")
	}
}

func (media *MediaResource) EditElement() {
	var id int
	fmt.Print("Введите Id: ")
	_, err := fmt.Fscan(os.Stdin, &id)

	if err != nil {
		return
	}

	item := media.MediaApiClient.GetPublicationById(id)

	if item != nil {
		item.Show()
	} else {
		fmt.Println("Публикация не найдена")
		return
	}

	publication, status := item.Read()

	if media.MediaApiClient.UpdatePublicationById(id, publication) && status {
		fmt.Println("Публикация успешно обновлена")
	} else {
		fmt.Println("Не удалось обновить публикацию")
	}
}

func (media *MediaResource) ShowList() {
	list := media.MediaApiClient.GetFeed()

	if len(list) == 0 {
		fmt.Println("Лента пуста")
	} else {
		for _, item := range media.MediaApiClient.GetFeed() {
			item.Show()
		}
	}
}

func (media *MediaResource) ShowElement() {
	var id int
	fmt.Print("Введите Id: ")

	_, err := fmt.Fscan(os.Stdin, &id)

	if err != nil {
		return
	}

	item := media.MediaApiClient.GetPublicationById(id)

	if item != nil {
		item.Show()
	} else {
		fmt.Println("Публикация не найдена")
	}
}

func (media *MediaResource) RemoveElement() {
	var id int
	fmt.Print("Введите Id: ")
	_, err := fmt.Fscan(os.Stdin, &id)

	if err != nil {
		return
	}

	if media.MediaApiClient.DeletePublicationById(id) == true {
		fmt.Println("Публикация успешно удалена")
	} else {
		fmt.Println("Не удалось удалить публикацию")
	}
}

func (media *MediaResource) ClearList() {
	if media.MediaApiClient.ClearFeed() {
		fmt.Println("Лента успешно очищена")
	} else {
		fmt.Println("Не удалось очистить ленту")
	}
}
