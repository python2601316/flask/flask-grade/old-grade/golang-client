package menu

import (
	"Lab5/aam2307/st28/media"
	"fmt"
)

type Menu struct {
	Media media.MediaResource
}

type MItem struct {
	title string
	f     func()
}

func (m *Menu) ShowMenu() bool {
	menuItems := m.collectMenu()

	for {
		for i, m := range menuItems {
			fmt.Println(i+1, m.title)
		}

		var i int
		if _, e := fmt.Scanln(&i); e != nil || i < 1 || i > len(menuItems) {
			return false
		}

		menuItems[i-1].f()
	}
}

func (m *Menu) collectMenu() []MItem {
	var menuItems = []MItem{
		{title: "Добавить", f: m.Media.AddElement},
		{title: "Редактировать", f: m.Media.EditElement},
		{title: "Показать список", f: m.Media.ShowList},
		{title: "Поиск по id", f: m.Media.ShowElement},
		{title: "Удалить по id", f: m.Media.RemoveElement},
		{title: "Очистить", f: m.Media.ClearList},
	}

	return menuItems
}
