package main

import (

	aam230728 "Lab5/aam2307/st28"
	"fmt"
	"sort"
)

var menuItems = []struct {
	title string
	f     func()
}{
	{"[2307-28] Скриплюк Виктор", aam230728.Main},
}

func menu() bool {

	for i, m := range menuItems {
		fmt.Println(i+1, m.title)
	}

	var i int
	if _, e := fmt.Scanln(&i); e != nil || i < 1 || i > len(menuItems) {
		return false
	}

	menuItems[i-1].f()
	return true
}

func main() {
	sort.Slice(menuItems, func(i, j int) bool {
		return menuItems[i].title < menuItems[j].title
	})
	for menu() {
	}
}
